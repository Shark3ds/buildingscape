// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerBox.h"
#include "OpenDoor.generated.h" //Por algum motivo que eu ainda n�o sei este cara tem que ser o ultimo...Trigger e Actor precisam estar acima dele

#define M_PRINT(x,y) if(GEngine){GEngine->AddOnScreenDebugMessage(-1, y, FColor::Red, x);}


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnOpenRequest); 

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnClosenRequest);
 

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BUILDINGSCAPE_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UOpenDoor();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void OpenDoor();
	void CloseDoor();

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


	UPROPERTY(BlueprintAssignable)
		FOnOpenRequest OnOpenRequest;

	UPROPERTY(BlueprintAssignable)
		FOnClosenRequest FOnClosenRequest;

	float GetOpenAngle();

private:

	UPROPERTY(EditAnyWhere)
		float OpenAngle = 0;

	UPROPERTY(EditAnyWhere)
		float DoorCloseDelay = 1.f;


	UPROPERTY(EditAnyWhere)
		ATriggerBox* PressurePlate;

	float LastDoorOpenTime;
	
	AActor* Owner;

	float GetTotalMassOnThePlate();

	

};
