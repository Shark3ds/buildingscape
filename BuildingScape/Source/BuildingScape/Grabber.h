// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/Engine.h"
#include <PhysicsEngine/PhysicsHandleComponent.h>
#include "Components/InputComponent.h"

#include "Grabber.generated.h" //R.Respect the generated file!


#define M_PRINT(x,y) if(GEngine){GEngine->AddOnScreenDebugMessage(-1, y, FColor::Red, x);}


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BUILDINGSCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UGrabber();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


private:

	float Reach = 150.f;

	UPhysicsHandleComponent* PhysicsHandle = nullptr;

	UInputComponent* PawnInputComponent = nullptr;;

	void Grab(); //Grab what's in reach of Pawn
	void Ungrab();

	const FHitResult GetFirstPhysicsBodyInReach();

	FVector GetReachLineEnd();

};
