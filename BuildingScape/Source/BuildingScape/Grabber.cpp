// Fill out your copyright notice in the Description page of Project Settings.

#include "Grabber.h"

#include <Engine/World.h>
#include <GameFramework/Actor.h>
#include "Components/InputComponent.h"
#include <Runtime/Engine/Public/DrawDebugHelpers.h>

#define OUT //R. Only for tell to us that UE will change the values...

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	// ...
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	//Look For Attached Physics Handle

	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	PawnInputComponent = GetOwner()->FindComponentByClass<UInputComponent>();

	if (PawnInputComponent) {

		///Bind Input
		PawnInputComponent->BindAction("Grab", EInputEvent::IE_Pressed, this, &UGrabber::Grab);
		PawnInputComponent->BindAction("Grab", EInputEvent::IE_Released, this, &UGrabber::Ungrab);

	}

}


void UGrabber::Grab() {
	if (!PhysicsHandle) { return; }
	FHitResult Hit = GetFirstPhysicsBodyInReach();
	if (Hit.GetComponent() != nullptr) {
		PhysicsHandle->GrabComponent(Hit.GetComponent(), "None", GetReachLineEnd(), true);
	}
}

void UGrabber::Ungrab() {
	if (!PhysicsHandle) { return; }
	PhysicsHandle->ReleaseComponent();
}

const FHitResult UGrabber::GetFirstPhysicsBodyInReach()
{

	//Draw the Line!
	DrawDebugLine(
		GetWorld(),
		GetOwner()->GetActorLocation(),
		GetReachLineEnd(),
		FColor(0, 0, 255),
		false, -1, 0,
		1
	);

	//RayCast

	FHitResult Hit;

	bool HitSomething = GetWorld()->LineTraceSingleByObjectType(
		OUT Hit,
		GetOwner()->GetActorLocation(),
		GetReachLineEnd(),
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		FCollisionQueryParams(FName(TEXT("")), false, GetOwner())
	);

	return Hit;
}

FVector UGrabber::GetReachLineEnd()
{

	//Ray-Cast Out to Reach Distance
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotator;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotator
	);

	//Vector() � o GetForwardVector do blueprint...
	FVector LineTraceEnd = PlayerViewPointLocation + (PlayerViewPointRotator.Vector() * Reach);

	return LineTraceEnd;
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!PhysicsHandle) { return; }

	if (PhysicsHandle->GrabbedComponent) {
		PhysicsHandle->SetTargetLocation(GetReachLineEnd());
	} 
}
