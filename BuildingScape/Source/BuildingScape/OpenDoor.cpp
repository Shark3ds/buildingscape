// Fill out your copyright notice in the Description page of Project Settings.

#include "OpenDoor.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "Runtime/Engine/Classes/Components/PrimitiveComponent.h"

float queijo = 12;

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

	Owner = GetOwner();
	
}

void UOpenDoor::OpenDoor()
{
//	GetOwner()->SetActorRotation(FRotator(0.f, 0.f, 0.f));
	OnOpenRequest.Broadcast();
}

void UOpenDoor::CloseDoor()
{

	//GetOwner()->SetActorRotation(FRotator(0.f, 90.f, 0.f));
	FOnClosenRequest.Broadcast();
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//Poll the trigger colume
		// if the actor that opons is in the volume

	if (GetTotalMassOnThePlate() > 40.f) {
		OpenDoor();

	}
	else {
		CloseDoor();
	}

}

float UOpenDoor::GetOpenAngle()
{
	return OpenAngle;
}


float UOpenDoor::GetTotalMassOnThePlate() {

	float TotalMass = 0.f;

	TArray<AActor*> OverLappingActors;
	if (PressurePlate) {
		PressurePlate->GetOverlappingActors(OUT OverLappingActors);
	}


	for (AActor* Actor : OverLappingActors)
	{
		TotalMass += Actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
	}

	FString Mass = FString::SanitizeFloat(TotalMass); //R.Convert to String...

	return TotalMass;

}